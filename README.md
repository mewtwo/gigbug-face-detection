# README #
### How to set up ###

* install numpy: `pip install numpy`
* install [pyTube](https://github.com/nficano/pytube): `pip install pytube`
* install [opencv3](http://opencv.org/downloads.html)

### How to run ###

* choose a Youtube video URL and take the unique ID after `www.youtube.com/watch?v=`
* choose an artist ID (this will be the output folder's name)
* run
	+ `python facesharpness.py <videoID> <artistID> [sharpness requirement (0-255, default 180)] [detection threshold (default 15)]` 
	+ OR `python facesFromVideos.py` after changing _artistID_ and _videoIDs_ parameters