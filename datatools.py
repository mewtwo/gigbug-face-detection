import io, json, urllib, time, datetime

def loadDataFromURL(url):
	if isinstance(url, unicode):
		url = url.encode('utf-8')
	try:
		response = urllib.urlopen(url);
	except IOError as ioe:
		print 'IOError at', url, '\n', str(ioe)
		return
	else:
		try:
			data = json.loads(response.read())
		except ValueError as ve:
			print 'ValueError at', url, '\n', str(ve)
			return
	return data

def readJSON(filename):
	with open(filename, 'r') as infile:
		return json.loads(infile.read())

def writeJSON(filename,content):
	with io.open(filename, 'w', encoding='utf-8') as outfile:
  		outfile.write(unicode(json.dumps(content, ensure_ascii=False)))
  	print filename, "written"

def aDuplicateArtist(elem, lst):
    for e in lst:
        if elem == e['clean_name']:
        	return True
    return False

def aDuplicate(elem, lst):
	for e in lst:
		if elem == e:
			return True
	return False

def containsCharacter(char,str):
    for c in str:
        if c == char:
            return True
    return False

def withoutBracketPart(str):
	for c in range(len(str)):
		if str[c] == '(':
			return str[:c-1]

def callAPIwithDelay(url,retryCount,sleepConstant):
	for i in range(retryCount):
		data = loadDataFromURL(url)
		if not isinstance(data,type(None)) and not 'error' in data:
			return data
		else:
			print '\tAPI limit exceeded - retrying', i
			time.sleep(sleepConstant)

def newTimestamp():
	return datetime.datetime.utcnow()
