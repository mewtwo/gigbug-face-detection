import cv2, os, sys, shutil, numpy
from pytube import YouTube
from datatools import loadDataFromURL

videoID = sys.argv[1]
artistID = sys.argv[2]

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def newDir(dirname):
	if os.path.exists(dirname):
		shutil.rmtree(dirname)
	os.makedirs(dirname)

def cwdFiles():
	return [f for f in os.listdir('.') if os.path.isfile(f)]

def removeTempDir():
	os.chdir(os.pardir)
	if os.path.exists(tempDir):
		shutil.rmtree(tempDir)

if len(sys.argv) < 3:
	print 'usage: python facesharpness.py <youtube videoID> <artistID> [sharpness threshold ([0-255] default 180)] [detection threshold (default 15)]'
	sys.exit()

if len(sys.argv) > 3:
	sharpnessReq = int(sys.argv[3])
else:
	sharpnessReq = 180
if len(sys.argv) > 4:
	neighborsReq = int(sys.argv[4])
else:
	neighborsReq = 15
tempDir = 'vid'
if not os.path.exists(tempDir):
	os.makedirs(tempDir)
newDir(artistID)

os.chdir(tempDir)
if not ('%s.mp4' % videoID) in cwdFiles():
	yt = YouTube()
	yt.from_url("http://www.youtube.com/watch?v=%s" % videoID)
	yt.set_filename(videoID)
	video = yt.filter('mp4')[-1]
	res = video.resolution
	if res != '1080p' and res != '720p' and res != '480p':
		print 'No resolution >= 480p found'
		removeTempDir()
		sys.exit()
	print 'Downloading...', res
	video.download(force_overwrite=True)
	print videoID, 'downloaded'
else:
	print videoID, 'already downloaded'

cap = cv2.VideoCapture('%s.mp4' % videoID)

faceFrames=[]
framecounter = 0
os.chdir(os.pardir)
os.chdir(artistID)

while(cap.isOpened()):
	framecounter = framecounter+1
	ret, frame = cap.read()  # read current frame
	if frame is None:
		break
	faces = face_cascade.detectMultiScale(frame, scaleFactor=1.4, minNeighbors=neighborsReq, minSize=(60,60))
	maxfacesharpness = 0
	for (x,y,w,h) in faces:
		facesharpness = numpy.max(cv2.convertScaleAbs(cv2.Laplacian(frame[y:y+h, x:x+w],3)))
		if facesharpness > sharpnessReq:
			cv2.imshow('%s-shrp: %s' % (str(framecounter),facesharpness),frame[y:y+h, x:x+w])
			cv2.waitKey(50)
		if facesharpness > maxfacesharpness:
			maxfacesharpness = facesharpness
	if maxfacesharpness > sharpnessReq:
		faceFrames.append(framecounter)
		print len(faceFrames), framecounter
		#filelocation = os.path.join('/%s' % str(artistID), str(len(faceFrames)))
		cv2.imwrite('%05d-%05d-%03d.jpg' % (len(faceFrames), framecounter, maxfacesharpness),frame)
		#'%s/%s' % (artistID/len(faceFrames)),frame)
		#cv2.imshow(str(framecounter),frame[y:y+h, x:x+w])
		#cv2.waitKey(50)

print len(cwdFiles()), "face images found"
for faceImg in cwdFiles():
	print faceImg

removeTempDir()






