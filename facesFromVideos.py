import cv2, os, sys, shutil, numpy, operator
from pytube import YouTube
from pprint import pprint
from datatools import loadDataFromURL

youtubeAPIkey = "AIzaSyCdJjNQUby9jD56c-p7Qhr0-ausbzdEp4Y"
videoIDs = ["YkxLBDSVVaI","LPg4EyBHvzQ","k-ihT0tTcxQ"]
artistID = "stillwave"
tempDir = 'vid'

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
sharpnessReq = 150 #180
neighborsReq = 10 #15
if len(sys.argv) > 1:
	sharpnessReq = int(sys.argv[1])
if len(sys.argv) > 2:
	neighborsReq = int(sys.argv[2])

def newDir(dirname):
	if os.path.exists(dirname):
		shutil.rmtree(dirname)
	os.makedirs(dirname)

def cwdFiles():
	return [f for f in os.listdir('.') if os.path.isfile(f)]

def removeTempDir():
	os.chdir(os.pardir)
	if os.path.exists(tempDir):
		shutil.rmtree(tempDir)

def getViewCount(vidID):
	youtubeURL = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=%s&fields=items&regionCode=GB&key=%s" % (vidID,youtubeAPIkey)
	data = loadDataFromURL(youtubeURL)
	if 'items' in data and len(data['items']) > 0:
		return (int)(data['items'][0]['statistics']['viewCount'])

def analyseVideo(video):
	faceFrames=[]
	framecounter = 0

	while(video.isOpened()):
		framecounter = framecounter+1
		ret, frame = cap.read()
		if frame is None:
			break
		faces = face_cascade.detectMultiScale(frame, scaleFactor=1.4, minNeighbors=neighborsReq, minSize=(60,60))
		maxfacesharpness = 0
		for (x,y,w,h) in faces:
			facesharpness = numpy.max(cv2.convertScaleAbs(cv2.Laplacian(frame[y:y+h, x:x+w],3)))
			if facesharpness > sharpnessReq:
				cv2.imshow('%s-shrp: %s' % (str(framecounter),facesharpness),frame[y:y+h, x:x+w])
				cv2.waitKey(50)
			if facesharpness > maxfacesharpness:
				maxfacesharpness = facesharpness
		if maxfacesharpness > sharpnessReq:
			faceFrames.append(framecounter)
			print len(faceFrames), framecounter
			cv2.imwrite('%05d-%05d-%03d.jpg' % (len(faceFrames), framecounter, maxfacesharpness),frame)

if not os.path.exists(tempDir):
	os.makedirs(tempDir)
newDir(artistID)

viewCounts = {}
highestResVid = {}

for vidID in videoIDs:
	viewCounts[vidID] = getViewCount(vidID)
	print vidID, ':', viewCounts[vidID], 'views'

sortedVids = viewCounts.keys()
sortedVids.sort(cmp=lambda a,b: cmp(viewCounts[b],viewCounts[a]))

for videoID in sortedVids:
	yt = YouTube()
	yt.from_url("http://www.youtube.com/watch?v=%s" % videoID)
	highestResVid[videoID] = yt.filter('mp4')[-1]
	res = highestResVid[videoID].resolution
	if res == '1080p' or res == '720p':
		video = yt.get('mp4',res)
		break

if isinstance(video,type(None)):
	for videoID in sortedVids:
		if highestResVid[videoID].resolution == '480p':
			video = yt.get('mp4','480p')
			break

if isinstance(video,type(None)):
	print 'No resolution >= 480p found'
	removeTempDir()
	sys.exit()

os.chdir(tempDir)
if not ('%s.mp4' % videoID) in cwdFiles():
	print 'Downloading...',videoID, res
	yt.set_filename(videoID)
	video.download(force_overwrite=True)
	print 'download complete'
else:
	print videoID, 'already downloaded'

cap = cv2.VideoCapture('%s.mp4' % videoID)
os.chdir(os.pardir)
os.chdir(artistID)

analyseVideo(cap)

print len(cwdFiles()), "face images found"
for faceImg in cwdFiles():
	print faceImg

removeTempDir()






